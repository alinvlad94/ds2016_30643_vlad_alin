<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 10/31/2016
  Time: 6:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/client.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
</head>
<body>


<div class="container ">
    <div class="jumbotron" style="color: #1b6d85;  margin-top:100px; height: 90%; box-shadow: 0px 4px 4px 0px #FFFFFF;">
        <h2>Hello Admin</h2>
        <button style="float:right;" type="button" class="btn btn-info" onclick="window.location.href='/LogoutServlet'">
            Logout
        </button>
        <div style=" float:left; margin-top:6%;" class="well-searchbox">
            <form class="form-horizontal" role="form" action="/flight" method="get">
                <div class="form-group">
                    <div class="col-md-8">
                        <left><h2>Search</h2></left>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Departure</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Departure City" name="departure_city" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Departure</label>
                    <div class="col-md-8">
                        <input type="date" class="form-control" placeholder="Departure Date" name="departure_date" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Arrival</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Arrival City" name="arrival_city" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Arrival</label>
                    <div class="col-md-8">
                        <input type="date" class="form-control" placeholder="Departure Date" name="arrival_date" required/>
                    </div>
                </div>
                <div class="col-sm-offset-4 col-sm-5">
                    <button type="submit" class="btn btn-success">Search</button>
                </div>
                <button type="button" onclick="window.location.href='/admin'" class="btn btn-success">All</button>
            </form>
        </div>
        <div style="margin-top:14%;float:right" class="right-container">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Airplane</th>
                    <th>Departure City</th>
                    <th>Departure Date</th>
                    <th>Arrival City</th>
                    <th>Arrival Date</th>
                </tr>
                </thead>
                <c:if test="${!empty error}">${error}</c:if>
                <c:forEach var="flight" items="${flights}">
                    <tbody>
                    <tr>
                        <td>${flight.airplane_type}</td>
                        <td>${flight.departure_city}</td>
                        <td>${flight.departure_date}</td>
                        <td>${flight.arrival_city}</td>
                        <td>${flight.arrival_date}</td>
                        <td><a href="/flight?update=1&flight_number=${flight.flight_number}">Update</a></td>
                        <td><a href="/flight?delete=1&flight_number=${flight.flight_number}">Del</a></td>
                    </tr>
                    </tbody>
                </c:forEach>
            </table>
            <button class="btn btn-primary btn-success" data-toggle="modal" data-target="#myModalForCreate">
                Add new
            </button>
            <c:if test="${!empty flight}">
                <div class="jumbotron">
                <form class="form-horizontal" role="form" action="/flight" method="post">
                    <input type="hidden" name="flight_number" value="${flight.flight_number}"
                    <div class="form-group">
                        <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="${flight.airplane_type}" name="airplane_type" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="${flight.departure_city}" name="departure_city" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input type="date" class="form-control" placeholder="${flight.departure_date}" name="departure_date" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="${flight.arrival_city}" name="arrival_city" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <input type="date" class="form-control" placeholder="${flight.arrival_date}" name="arrival_date" required/>
                        </div>
                    </div>
                    <div class="col-sm-offset-4 col-sm-5">
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                </form>
                </div>
            </c:if>
        </div>
    </div>


    <%--Modal model --%>
    <div class="modal fade" id="myModalForCreate" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <h2>Add a new flight</h2>
                </div>
                <form action="/flight" method="post">
                    <div class="form-group">
                        <label for="airplane_type">Airplane type:</label>
                        <input type="text" class="form-control" id="airplane_type" name="airplane_type">
                    </div>
                    <div class="form-group">
                        <label for="departure_city">Departure city:</label>
                        <input type="text" class="form-control" id="departure_city" name="departure_city">
                    </div>
                    <div class="form-group">
                        <label for="departure_date">Departure date:</label>
                        <input type="date" class="form-control" id="departure_date" name="departure_date">
                    </div>
                    <div class="form-group">
                        <label for="arrival_city">Arrival city:</label>
                        <input type="text" class="form-control" id="arrival_city" name="arrival_city">
                    </div>
                    <div class="form-group">
                        <label for="arrival_date">Arrival date:</label>
                        <input type="date" class="form-control" id="arrival_date" name="arrival_date">
                    </div>
                    <input style = "margin-left:15px"  type="submit" class="btn btn-default btn-success"/>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <%--Modal model for update--%>
    <div class="modal fade" id="myModalForUpdate" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <h2>Add a new flight</h2>
                </div>
                <form action="/flight" method="post">
                    <div class="form-group">
                        <label for="airplane_type">Airplane type:</label>
                        <input type="text" class="form-control" id="airplane_type" name="airplane_type">
                    </div>
                    <div class="form-group">
                        <label for="departure_city">Departure city:</label>
                        <input type="text" class="form-control" id="departure_city" name="departure_city">
                    </div>
                    <div class="form-group">
                        <label for="departure_date">Departure date:</label>
                        <input type="date" class="form-control" id="departure_date" name="departure_date">
                    </div>
                    <div class="form-group">
                        <label for="arrival_city">Arrival city:</label>
                        <input type="text" class="form-control" id="arrival_city" name="arrival_city">
                    </div>
                    <div class="form-group">
                        <label for="arrival_date">Arrival date:</label>
                        <input type="date" class="form-control" id="arrival_date" name="arrival_date">
                    </div>
                    <input style = "margin-left:15px"  type="submit" class="btn btn-default btn-success"/>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%--Model for update ends here--%>




</div>
</body>
</html>