package com.flightmanager.dao;

import com.flightmanager.entity.City;
import com.flightmanager.entity.Flight;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alin on 11/2/2016.
 */
public class CityDao {

    private SessionFactory factory;

    public CityDao(SessionFactory factory){
        this.factory = factory;
    }

    public City getByCity(String city){

        List<City> cities = new ArrayList<City>();
        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE city =:city");
            query.setParameter("city", city);
            cities = query.list();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
        return cities.get(0);
    }
}
