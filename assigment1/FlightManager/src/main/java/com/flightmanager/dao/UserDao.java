package com.flightmanager.dao;

import com.flightmanager.entity.Authority;
import com.flightmanager.entity.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.criteria.internal.CriteriaDeleteImpl;

/**
 * Created by Alin on 10/31/2016.
 */
public class UserDao {

    private SessionFactory factory;

    public UserDao(SessionFactory factory){
        this.factory = factory;
    }

    public User addUser(User user){
        Authority authority = new Authority();
        authority.setUsername(user.getUsername());
        authority.setAuthority("ROLE_CLIENT");
        //Add authority first - username in Authorities(table) is FK
        addAuthorityFirst(authority);

        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
        }catch (HibernateException e){
            if(tx != null){
                tx.rollback();
            }
        }finally {
            session.close();
        }
        return user;
    }

    public Authority addAuthorityFirst(Authority authority){
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(authority);
            tx.commit();
        }catch (HibernateException e){
            if(tx != null){
                tx.rollback();
            }
        }finally {
            session.close();
        }
        return authority;
    }

}
