package com.flightmanager.dao;

import com.flightmanager.entity.Flight;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alin on 11/1/2016.
 */
public class FlightDao {

    private SessionFactory factory;

    public FlightDao(SessionFactory factory){
        this.factory = factory;
    }

    public Flight addFlight(Flight flight){

        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(flight);
            tx.commit();
        }catch (HibernateException e){
            if(tx != null){
                tx.rollback();
            }
        }finally {
            session.close();
        }
        return flight;
    }

    public List<Flight> getAllFlights(){

        List<Flight> flights = new ArrayList<Flight>();
        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight");
            flights = query.list();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
        return flights;
    }

    public List<Flight> getByDetails(Flight flight){

        List<Flight> flights = new ArrayList<Flight>();
        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE departure_city =:departure_city AND departure_date =:departure_date AND arrival_city =:arrival_city AND arrival_date =:arrival_date");
            query.setParameter("departure_city", flight.getDeparture_city());
            query.setParameter("departure_date", flight.getDeparture_date());
            query.setParameter("arrival_city", flight.getArrival_city());
            query.setParameter("arrival_date", flight.getArrival_date());
            flights = query.list();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
        return flights;
    }

    public Flight getByFlightNumber(int flight_number){

        List<Flight> flights = new ArrayList<Flight>();
        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE flight_number =:flight_number");
            query.setParameter("flight_number", flight_number);
            flights = query.list();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
        return flights.get(0);
    }

    public void updateFlight(Flight flight) {

        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("UPDATE Flight SET departure_city =:departure_city, " +
                                             "departure_date =:departure_date, arrival_city =:arrival_city " +
                                              ", arrival_date =:arrival_date  WHERE flight_number =:flight_number");
            query.setParameter("flight_number", flight.getFlight_number());
            query.setParameter("departure_city", flight.getDeparture_city());
            query.setParameter("departure_date", flight.getDeparture_date());
            query.setParameter("arrival_city", flight.getArrival_city());
            query.setParameter("arrival_date", flight.getArrival_date());
            query.executeUpdate();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
    }

    public void deleteFlight(int flight_number) {

        Session session = factory.openSession();
        Transaction tx = null;
        Flight flightToBeDeleted = getByFlightNumber(flight_number);

        try{
            tx = session.beginTransaction();
            session.delete(flightToBeDeleted);
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
    }
}
