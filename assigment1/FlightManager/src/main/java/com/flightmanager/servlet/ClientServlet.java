package com.flightmanager.servlet;

import com.flightmanager.dao.FlightDao;
import com.flightmanager.dao.UserDao;
import com.flightmanager.entity.Flight;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Alin on 10/31/2016.
 */
public class ClientServlet extends HttpServlet {

    private FlightDao flightDao;
    private static final String CLIENT_URL = "/WEB-INF/jsp/client.jsp";

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
        super.service(req, resp);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if(req.getParameter("localtime") != null){
            req.setAttribute("localtime", req.getParameter("localtime"));
        }
        List<Flight> flights = flightDao.getAllFlights();
        req.setAttribute("flights", flights);
        req.getRequestDispatcher(CLIENT_URL).forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }
}
