package com.flightmanager.servlet.resource;

import com.flightmanager.dao.FlightDao;
import com.flightmanager.entity.Flight;
import org.hibernate.cfg.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Alin on 11/1/2016.
 */
public class FlightServlet extends HttpServlet {

    private FlightDao flightDao;

    private static final String CLIENT_URL = "/WEB-INF/jsp/client.jsp";
    private static final String ADMIN_URL = "/WEB-INF/jsp/admin.jsp";
    private static final String ERROR_URL = "/WEB-INF/jsp/403.jsp";

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
        super.service(req, resp);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if(req.getParameter("departure_city") != null) {
            List<Flight> flights = null;
            try {
                flights = flightDao.getByDetails(buildFlight(req));
                req.setAttribute("flights", flights);
            } catch (ParseException e) {
                req.setAttribute("error", e.getMessage());
            }
            redirectByRole(req, resp);
        }else if(req.getParameter("flight_number") != null && req.getParameter("delete") != null){
            int flight_number = Integer.parseInt(req.getParameter("flight_number"));
            flightDao.deleteFlight(flight_number);
            redirectByRole(req, resp);
        }else if(req.getParameter("flight_number") != null && req.getParameter("update") != null) {
            int flight_number = Integer.parseInt(req.getParameter("flight_number"));
            redirectForUpdate(req, resp);
        }else{
            redirectByRole(req, resp);
        }
    }



    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        try {
            Flight flight = buildFlight(req);
            String flight_number = req.getParameter("flight_number") != null ? req.getParameter("flight_number") : null;
            if(flight_number == null){
                flight.setAirplane_type(req.getParameter("airplane_type"));
                createFlight(flight);
                redirectByRole(req, resp);
            }else{
                flight.setFlight_number(Integer.parseInt(flight_number));
                updateFlight(flight);
                redirectByRole(req, resp);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void redirectByRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) req.getUserPrincipal();
        SimpleGrantedAuthority ROLE_CLIENT = new SimpleGrantedAuthority("ROLE_CLIENT");
        SimpleGrantedAuthority ROLE_ADMIN = new SimpleGrantedAuthority("ROLE_ADMIN");
        boolean isClient = usernamePasswordAuthenticationToken.getAuthorities().contains(ROLE_CLIENT);
        boolean isAdmin = usernamePasswordAuthenticationToken.getAuthorities().contains(ROLE_ADMIN);

        if(isClient){
            //resp.sendRedirect("/client");
            req.getRequestDispatcher(CLIENT_URL).forward(req, resp);
        }else if(isAdmin){
            //resp.sendRedirect("/admin");
            req.getRequestDispatcher(ADMIN_URL).forward(req, resp);
        }else {
            req.getRequestDispatcher(ERROR_URL).forward(req, resp);
        }
    }

    private void redirectForUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) req.getUserPrincipal();
        SimpleGrantedAuthority ROLE_CLIENT = new SimpleGrantedAuthority("ROLE_CLIENT");
        SimpleGrantedAuthority ROLE_ADMIN = new SimpleGrantedAuthority("ROLE_ADMIN");
        boolean isClient = usernamePasswordAuthenticationToken.getAuthorities().contains(ROLE_CLIENT);
        boolean isAdmin = usernamePasswordAuthenticationToken.getAuthorities().contains(ROLE_ADMIN);

        if(isClient){
            resp.sendRedirect("/client");
            //req.getRequestDispatcher(CLIENT_URL).forward(req, resp);
        }else if(isAdmin){
            resp.sendRedirect("/admin?flight_number=" + req.getParameter("flight_number") + "&update=1");
            //req.getRequestDispatcher(ADMIN_URL).forward(req, resp);
        }else {
            req.getRequestDispatcher(ERROR_URL).forward(req, resp);
        }
    }

    private void createFlight(Flight flight) {
        flightDao.addFlight(flight);
    }

    private void updateFlight(Flight flight) {
        flightDao.updateFlight(flight);
    }


    private Flight buildFlight(HttpServletRequest req) throws ParseException {
        Flight flight = new Flight();

        String departure_city = req.getParameter("departure_city");
        String departure_date = req.getParameter("departure_date");
        String arrival_city = req.getParameter("arrival_city");
        String  arrival_date = req.getParameter("arrival_date");

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date departureDate = format.parse(departure_date);
        java.sql.Date departureSqlDate = new java.sql.Date(departureDate.getTime());
        Date arrivalDate = format.parse(arrival_date);
        java.sql.Date arrivalSqlDate = new java.sql.Date(arrivalDate.getTime());

        flight.setDeparture_city(departure_city);
        flight.setDeparture_date(departureSqlDate);
        flight.setArrival_city(arrival_city);
        flight.setArrival_date(arrivalSqlDate);

        return flight;
    }
}
