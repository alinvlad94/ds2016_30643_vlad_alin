package com.flightmanager.servlet.resource;

import com.flightmanager.dao.CityDao;
import com.flightmanager.dao.FlightDao;
import com.flightmanager.dao.UserDao;
import com.flightmanager.entity.City;
import com.flightmanager.entity.Timezone;
import com.flightmanager.service.TimezoneService;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alin on 11/2/2016.
 */
public class TimezoneServlet extends HttpServlet {

    private CityDao cityDao;
    private FlightDao flightDao;
    private TimezoneService timezoneService;
    private static final String ADMIN_URL = "/WEB-INF/jsp/admin.jsp";

    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
        flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
        timezoneService = new TimezoneService();
        super.service(req, resp);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if(req.getParameter("flight_number") == null){
            resp.sendRedirect("/client");
        }
        int flight_number = Integer.parseInt(req.getParameter("flight_number"));
        String departure_city = flightDao.getByFlightNumber(flight_number).getDeparture_city();
        String arrival_city = flightDao.getByFlightNumber(flight_number).getArrival_city();
        City departure_city_coord = cityDao.getByCity(departure_city);
        String localtime = timezoneService.getTimezone(departure_city_coord);
        req.setAttribute("localtime", localtime);
        resp.sendRedirect("/client?localtime="+localtime);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }

}
