package com.flightmanager.servlet;

import com.flightmanager.dao.FlightDao;
import com.flightmanager.dao.UserDao;
import com.flightmanager.entity.Flight;
import com.flightmanager.entity.User;
import com.flightmanager.service.TimezoneService;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Alin on 10/31/2016.
 */
public class AdminServlet extends HttpServlet {

    private UserDao userDao;
    private FlightDao flightDao;
    private static final String ADMIN_URL = "/WEB-INF/jsp/admin.jsp";

    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        userDao = new UserDao(new Configuration().configure().buildSessionFactory());
        flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
        super.service(req, resp);
    }


    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if(req.getParameter("flight_number") != null && req.getParameter("update") != null){
            Flight flight = flightDao.getByFlightNumber(Integer.parseInt(req.getParameter("flight_number")));
            req.setAttribute("flight", flight);
        }
        List<Flight> flights = flightDao.getAllFlights();
        req.setAttribute("flights", flights);
        req.getRequestDispatcher(ADMIN_URL).forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }
}
