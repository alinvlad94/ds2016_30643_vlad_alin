package com.flightmanager.servlet;

import com.sun.javafx.UnmodifiableArrayList;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

/**
 * Created by Alin on 10/31/2016.
 */
public class LoginServlet extends HttpServlet {

    private static final String ADMIN_URL = "/WEB-INF/jsp/admin.jsp";
    private static final String CLIENT_URL = "/WEB-INF/jsp/client.jsp";
    private static final String ERROR_URL = "/WEB-INF/jsp/403.jsp";

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) req.getUserPrincipal();
        SimpleGrantedAuthority ROLE_CLIENT = new SimpleGrantedAuthority("ROLE_CLIENT");
        SimpleGrantedAuthority ROLE_ADMIN = new SimpleGrantedAuthority("ROLE_ADMIN");
        boolean isClient = usernamePasswordAuthenticationToken.getAuthorities().contains(ROLE_CLIENT);
        boolean isAdmin = usernamePasswordAuthenticationToken.getAuthorities().contains(ROLE_ADMIN);

        if(isClient){
            resp.sendRedirect("/client");
        }else if(isAdmin){
            resp.sendRedirect("/admin");
        }else {
            req.getRequestDispatcher(ERROR_URL).forward(req, resp);
        }

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        doGet(req, resp);

    }
}
