package com.flightmanager.service;

import com.flightmanager.entity.City;
import com.flightmanager.entity.Timezone;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Alin on 10/31/2016.
 */
@Service("timezoneService")
public class TimezoneService {

    public String getTimezone(City city){

        String url = "http://www.new.earthtools.org/timezone/" + city.getLatitude() + "/" + city.getLongitude();
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Timezone> response = restTemplate.getForEntity(url, Timezone.class);
        return response.getBody().getLocaltime();
    }
}
