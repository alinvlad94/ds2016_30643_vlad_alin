package com.flightmanager.entity;

/**
 * Created by Alin on 11/1/2016.
 */
public class Authority {

    private String username;
    private String authority;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
