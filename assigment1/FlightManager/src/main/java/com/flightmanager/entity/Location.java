package com.flightmanager.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Alin on 10/31/2016.
 */
@XmlRootElement
public class Location {

    private double latitude;
    private double longitude;

    public Location() {
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
