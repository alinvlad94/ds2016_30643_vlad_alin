<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 10/31/2016
  Time: 6:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/client.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
</head>
<body>


<div class="container ">
    <div class="jumbotron" style="color: #1b6d85;  margin-top:100px; height: 90%; box-shadow: 0px 4px 4px 0px #FFFFFF;">
        <button style="float:right;" type="button" class="btn btn-info" onclick="window.location.href='/LogoutServlet'">
            Logout
        </button>
        <div style=" float:left; margin-top:6%;" class="well-searchbox">
            <form class="form-horizontal" role="form" action="/flight" method="get">
                <div class="form-group">
                    <div class="col-md-8">
                        <left><h2>Search</h2></left>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Country</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Departure City" name="departure_city" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Province</label>
                    <div class="col-md-8">
                        <input type="date" class="form-control" placeholder="Departure Date" name="departure_date" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">City</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Arrival City" name="arrival_city" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Category</label>
                    <div class="col-md-8">
                        <input type="date" class="form-control" placeholder="Departure Date" name="arrival_date" required/>
                    </div>
                </div>
                <div class="col-sm-offset-4 col-sm-5">
                    <button type="submit" class="btn btn-success">Search</button>
                </div>
                <button type="button" onclick="window.location.href='/client'" class="btn btn-success">All</button>
            </form>
        </div>
        <div style="margin-top:13%;float:right" class="right-container">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Airplane</th>
                    <th>Departure City</th>
                    <th>Departure Date</th>
                    <th>Arrival City</th>
                    <th>Arrival Date</th>
                </tr>
                </thead>
                <c:if test="${!empty error}">${error}</c:if>
                <c:forEach var="flight" items="${flights}">
                    <tbody>
                    <tr>
                        <td><a href="/timezone?flight_number=${flight.flight_number}">${flight.airplane_type}</a></td>
                        <td>${flight.departure_city}</td>
                        <td>${flight.departure_date}</td>
                        <td>${flight.arrival_city}</td>
                        <td>${flight.arrival_date}</td>
                    </tr>
                    </tbody>
                </c:forEach>
                <c:if test="${!empty localtime}">
                    <h3>Localtime for departure city: ${localtime} </h3>
                </c:if>

            </table>
        </div>
    </div>

</div>
</body>
</html>
