package launcher;

import service.packetservice.PacketService;

import javax.xml.ws.Endpoint;

/**
 * @author Alin - 12/19/2016.
 */
public class Launcher {

    public static void main(String[] args){

        Object packetService = new PacketService();
        String address = "http://localhost:9000/PacketService";

        Endpoint.publish(address, packetService);
    }
}
