
package service.packetservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "addPackageResponse", namespace = "http://packetservice.service/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addPackageResponse", namespace = "http://packetservice.service/")
public class AddPackageResponse {


}
