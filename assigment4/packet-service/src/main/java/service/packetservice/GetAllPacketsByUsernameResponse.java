
package service.packetservice;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getAllPacketsByUsernameResponse", namespace = "http://packetservice.service/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAllPacketsByUsernameResponse", namespace = "http://packetservice.service/")
public class GetAllPacketsByUsernameResponse {

    @XmlElement(name = "return", namespace = "")
    private List<model.Packet> _return;

    /**
     * 
     * @return
     *     returns List<Packet>
     */
    public List<model.Packet> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<model.Packet> _return) {
        this._return = _return;
    }

}
