package service.packetservice;

import javax.jws.WebMethod;
import javax.jws.WebService;

import handler.PacketHandler;
import model.CustomMap;
import model.Packet;
import model.Route;

import java.util.List;

/**
 * @author Alin - 12/19/2016.
 */
@WebService
public class PacketService {

    private PacketHandler packetHandler = new PacketHandler();

    /**
     * Admin authentication required
     * @param pack
     */
    @WebMethod
    public void addPackage(Packet pack){
        packetHandler.addPacket(pack);
    }

    /**
     * Admin authentication required
     * @param idPacket
     */
    @WebMethod
    public void removePackage(int idPacket){
        packetHandler.removePacket(idPacket);
    }

    /**
     * Admin authentication required
     * @param idPacket
     * @param route
     */
    @WebMethod
    public void registerPackage(int idPacket, Route route){
        packetHandler.registerPackage(idPacket, route);
    }

    /**
     * Admin authentication required
     * @param idPacket
     * @param route
     */
    @WebMethod
    public void addNewRouteToPackage(int idPacket, Route route){
        packetHandler.addNewRouteToPackage(idPacket, route);
    }

    /**
     * Client operation
     * @return
     */
    @WebMethod
    public List<Packet> getAllPacketsByUsername(String username){
        return packetHandler.getAllPacketsByUsername(username);
    }

    /**
     * Client operation
     * @return
     */
    @WebMethod
    public List<Packet> getAllPacketsByUsernameAndName(String username, String packetName){
        return packetHandler.getAllPacketsByUsernameAndName(username, packetName);
    }

    /**
     * Client operation
     * @return
     */
    @WebMethod
    public List<Packet> getAllPackets(){
        return packetHandler.getAllPackets();
    }

    /**
     * Client operation
     * @param idPacket
     * @return
     */
    @WebMethod
    public CustomMap checkStatus(int idPacket){
        return packetHandler.checkStatus(idPacket);
    }

}
