
package service.packetservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "addPackage", namespace = "http://packetservice.service/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addPackage", namespace = "http://packetservice.service/")
public class AddPackage {

    @XmlElement(name = "arg0", namespace = "")
    private model.Packet arg0;

    /**
     * 
     * @return
     *     returns Packet
     */
    public model.Packet getArg0() {
        return this.arg0;
    }

    /**
     * 
     * @param arg0
     *     the value for the arg0 property
     */
    public void setArg0(model.Packet arg0) {
        this.arg0 = arg0;
    }

}
