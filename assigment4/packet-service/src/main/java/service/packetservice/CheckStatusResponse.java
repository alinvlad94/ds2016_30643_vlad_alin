
package service.packetservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "checkStatusResponse", namespace = "http://packetservice.service/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkStatusResponse", namespace = "http://packetservice.service/")
public class CheckStatusResponse {

    @XmlElement(name = "return", namespace = "")
    private model.CustomMap _return;

    /**
     * 
     * @return
     *     returns CustomMap
     */
    public model.CustomMap getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(model.CustomMap _return) {
        this._return = _return;
    }

}
