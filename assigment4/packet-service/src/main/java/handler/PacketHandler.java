package handler;

import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import dao.PacketDao;
import dao.RouteDao;
import model.CustomMap;
import model.Packet;
import model.Route;

import java.util.List;

/**
 * @author Alin - 12/20/2016.
 */
public class PacketHandler {

    private PacketDao packetDao;
    private RouteDao routeDao;

    /**
     * Constructor -> Initialize DAO objs that we are going to use
     */
    public PacketHandler(){
        packetDao = new PacketDao();
        routeDao = new RouteDao();
    }

    public List<Packet> getAllPackets(){
        return packetDao.getAllPackets();
    }

    public List<Packet> getAllPacketsByUsername(String username){
        return packetDao.getAllPacketsByUsername(username);
    }

    public List<Packet> getAllPacketsByUsernameAndName(String username, String packetName){
        return packetDao.getAllPacketsByUsernameAndName(username, packetName);
    }

    public void addPacket(Packet packet){
        packetDao.addPacket(packet);
    }

    public void removePacket(int idPacket){
        packetDao.removePacket(idPacket);
    }

    public void registerPackage(int idPackage, Route route){
        packetDao.makePacketAvailable(idPackage);
        routeDao.insertRoute(idPackage, route);
    }

    public void addNewRouteToPackage(int idPackage, Route route){
        //We want to make tracking = 'Y' for the packet that we added a route for
        //We using this even from 2 flows - Add route to packet, And register packet
        Packet packet = packetDao.getPacketById(idPackage);
        packet.setTracking(true);
        packetDao.removePacket(idPackage);
        packetDao.addPacketWithSameId(packet);

        routeDao.insertRoute(idPackage, route);
    }

    public CustomMap checkStatus(int idPackage){
        List<Route> routes = routeDao.getRoutesByPacket(idPackage);

        CustomMap customMap = new CustomMap();
        customMap.setPacket(packetDao.getPacketById(idPackage));
        customMap.setRoutes(routes);

        return customMap;
    }
}
