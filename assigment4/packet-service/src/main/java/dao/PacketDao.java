package dao;

import dao.generic.GenericDao;
import model.Packet;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import java.util.stream.Collectors;

/**
 * @author Alin - 12/19/2016.
 */
public class PacketDao extends GenericDao {

    /**
     * Constructor -> call the super() method of GenericDao where we initially construct the dataSource and the NamedParameterJdbcTemplate object
     */
    public PacketDao() {
        super();
    }

    /**
     * @return all packets in the database
     */
    public List<Packet> getAllPackets() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        return namedParameterJdbcTemplate.query("select * from packet", params, new PacketRowMapper());
    }

    /**
     * @param username
     * @return all packets from the database where the username is either the sender or the receiver
     */
    public List<Packet> getAllPacketsByUsername(String username) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("sender", username)
                .addValue("receiver", username);
        return namedParameterJdbcTemplate.query("select * from packet where sender = :sender or receiver = :receiver", params, new PacketRowMapper());
    }

    /**
     * @param username
     * @param nameOfPacket
     * @return packets for a certain username with a specified packet name
     */
    public List<Packet> getAllPacketsByUsernameAndName(String username, String nameOfPacket) {
        return getAllPacketsByUsername(username).stream().filter(e -> e.getName().equals(nameOfPacket)).collect(Collectors.toList());
    }

    public Packet getPacketById(int idPacket) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id_packet", idPacket);
        return namedParameterJdbcTemplate.query("select * from packet where id_packet = :id_packet", params, new PacketRowMapper()).get(0);
    }

    /**
     * Add new packet
     * Used from the admin console
     * @param packet
     */
    public void addPacket(Packet packet) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("sender", packet.getSender())
                .addValue("receiver", packet.getReceiver())
                .addValue("name", packet.getName())
                .addValue("description", packet.getDescription())
                .addValue("sender_city", packet.getSender_city())
                .addValue("receiver_city", packet.getReceiver_city())
                .addValue("tracking", packet.isTracking() == true ? "Y" : "N");

        namedParameterJdbcTemplate.update("insert into packet(sender,receiver,name,description,sender_city,receiver_city,tracking) " +
                "values(:sender, :receiver, :name, :description, :sender_city, :receiver_city, :tracking);", params);
    }

    /**
     * Remove packet by id_packet
     * Used from admin console
     * @param idPacket
     */
    public void removePacket(int idPacket){
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id_packet", idPacket);
        namedParameterJdbcTemplate.update("delete from packet where id_packet = :id_packet", params);
    }

    /**
     *
     * @param idPacket
     */
    public void makePacketAvailable(int idPacket){
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id_packet", idPacket);
        namedParameterJdbcTemplate.update("update packet set tracking = true where id_packet = :id_packet;", params);
    }

    public void addPacketWithSameId(Packet packet) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id_packet", packet.getId_package())
                .addValue("sender", packet.getSender())
                .addValue("receiver", packet.getReceiver())
                .addValue("name", packet.getName())
                .addValue("description", packet.getDescription())
                .addValue("sender_city", packet.getSender_city())
                .addValue("receiver_city", packet.getReceiver_city())
                .addValue("tracking", packet.isTracking() == true ? "Y" : "N");

        namedParameterJdbcTemplate.update("insert into packet(id_packet, sender,receiver,name,description,sender_city,receiver_city,tracking) " +
                "values(:id_packet, :sender, :receiver, :name, :description, :sender_city, :receiver_city, :tracking);", params);
    }

    /**
     * RowMapperClass used in interogations for mapping the resultSet to a Packet ORM
     */
    private class PacketRowMapper implements RowMapper<Packet> {

        @Override
        public Packet mapRow(ResultSet resultSet, int i) throws SQLException {
            Packet packet = new Packet();
            packet.setId_package(resultSet.getInt("id_packet"));
            packet.setName(resultSet.getString("name"));
            packet.setDescription(resultSet.getString("description"));
            packet.setReceiver(resultSet.getString("receiver"));
            packet.setReceiver_city(resultSet.getNString("receiver_city"));
            packet.setSender(resultSet.getString("sender"));
            packet.setSender_city(resultSet.getString("sender_city"));
            packet.setTracking(resultSet.getString("tracking").equals("Y") ? true : false);
            return packet;
        }
    }
}
