package dao.generic;

import dao.datasource.DataSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * @author Alin - 12/19/2016.
 */
public abstract class GenericDao {

    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    protected DataSource dataSource;

    public GenericDao(){
        dataSource = DataSource.getInstance();
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
}
