package dao;

import dao.generic.GenericDao;
import model.Route;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * @author Alin - 12/19/2016.
 */
public class RouteDao extends GenericDao{

    public RouteDao(){
        super();
    }

    public void insertRoute(int idPacket, Route route){
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id_packet", idPacket)
                .addValue("city", route.getCity())
                .addValue("time", route.getTime()!= null ? route.getTime() : new Date());
        namedParameterJdbcTemplate.update("insert into routes (id_packet, city, time) values (:id_packet, :city, :time);", params);
    }

    public List<Route> getRoutesByPacket(int idPacket){
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id_packet", idPacket);
        return namedParameterJdbcTemplate.query("select * from routes where id_packet = :id_packet", params, new RouteRowMapper());
    }

    private class RouteRowMapper implements RowMapper<Route>{

        @Override
        public Route mapRow(ResultSet resultSet, int i) throws SQLException {
            Route route = new Route();
            route.setCity(resultSet.getString("city"));
            route.setTime(resultSet.getTime("time"));
            return route;
        }
    }

}
