package model;

import java.util.Date;

/**
 * @author Alin - 12/19/2016.
 */
public class Route {


    private String city;
    private Date time;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
