package model;

import java.io.Serializable;
import java.io.Serializable;
import java.util.List;

/**
 * @author Alin - 12/21/2016.
 */
public class CustomMap implements Serializable {

    private List<Route> routes;
    private Packet packet;

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public Packet getPacket() {
        return packet;
    }

    public void setPacket(Packet packet) {
        this.packet = packet;
    }
}
