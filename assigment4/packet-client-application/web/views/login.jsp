<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 12/11/2016
  Time: 8:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href='<c:url value="/css/bootstrap.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/login.css"/>' rel="stylesheet">
    <script src='<c:url value="/js/jquery.js"/>'></script>
    <script src='<c:url value="/js/bootstrap.js"/>'></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
</head>
<body>
<center>
    <div class="login-page">

        <div class="form">
            <form class="login-form" action="/j_spring_security_check" method="post">
                <input type="text" name="j_username" placeholder="username"/>
                <input type="password" name="j_password" placeholder="password"/>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>.
                <button>login</button>
                <p class="message">Not registered? <a href="/register">Create an account</a></p>

                <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
                    <font color="red">
                        Your login attempt was not successful due to <br/><br/>
                        <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>.
                        <c:remove var="SPRING_SECURITY_LAST_EXCEPTION"/>
                    </font>
                </c:if>

            </form>
        </div>
    </div>
</center>
</body>
</html>
