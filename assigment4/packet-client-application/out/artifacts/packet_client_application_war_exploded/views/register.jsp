<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 12/19/2016
  Time: 10:05 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href='<c:url value="/css/bootstrap.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/login.css"/>' rel="stylesheet">
    <script src='<c:url value="/js/jquery.js"/>'></script>
    <script src='<c:url value="/js/bootstrap.js"/>'></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
</head>
<body>


<div class="login-page">

    <div class="form">
        <center><h4>Register</h4></center>
        <form class="login-form" action="/register" method="post">
            <input type="text" name="username" placeholder="Username" required/><br>
            <input type="password" name="password" placeholder="Password" required/><br>
            <input type="text" name="adress" placeholder="Adress" required/><br>
            <input type="text" name="phone" placeholder="Phone Number" required/><br>
            <input type="email" name="email" placeholder="Email" required/><br>
            <input type="hidden" name="role" value="ROLE_CLIENT"/>
            <button>Register</button>
            <h3 style="color: red">${error}</h3>
        </form>
    </div>
</div>

</body>
</html>
