<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 12/13/2016
  Time: 9:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <head>
        <link href='<c:url value="/css/bootstrap.css"/>' rel="stylesheet">
        <link href='<c:url value="/css/admin.css"/>' rel="stylesheet">
        <script src='<c:url value="/js/jquery.js"/>'></script>
        <script src='<c:url value="/js/bootstrap.js"/>'></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin</title>
    </head>
<body>
<div class="container">
    <div class="jumbotron" style="height : 400px; padding: auto; margin:auto; margin-top:15%;
            box-shadow: 1px 2px 2px 1px #91a5ae;">
        <div class="well small">
            <a style="font-size: 18px;" href="/logout">Logout</a>
        </div>

        <div class="box col-lg-8 col-md-8 col-sm-12 col-sm-12">
            <div style="overflow: scroll; height:220px">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Sender</th>
                        <th>Receiver</th>
                        <th>S City</th>
                        <th>R City</th>
                        <th>Tracked</th>
                        <th>Del</th>
                    </tr>
                    </thead>
                    <tbody style="height:150px; overflow-y:auto">
                    <c:forEach items="${packets}" var="packet">
                        <tr>
                            <td>${packet.name}</td>
                            <td>${packet.sender}</td>
                            <td>${packet.receiver}</td>
                            <td>${packet.senderCity}</td>
                            <td>${packet.receiverCity}</td>
                            <c:if test="${packet.tracking == true}">
                                <td>
                                    <a id='${packet.idPackage}' href="#" data-toggle="modal"
                                       data-target="#register-package"
                                       data-id="${packet.idPackage}">Add Route</a>
                                </td>
                            </c:if>
                            <c:if test="${packet.tracking == false}">
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#register-package"
                                       data-id="${packet.idPackage}">Activate</a>
                                </td>
                            </c:if>
                            <td><a href="/admin/delete-package/${packet.idPackage}"><span
                                    class="glyphicon glyphicon-remove-sign"></span></a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box col-lg-4 col-md-4 col-sm-12 col-sm-12">
            <button class="btn btn-lg btn-success" data-toggle="modal" data-target="#add-package">Add Package</button>
        </div>
    </div>

    <!-- Modal for Register new Package-->
    <div class="modal fade" id="register-package" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="loginmodal-container">
                <h1>Add Route / Activate Package</h1><br>
                <form action="/admin/add-route" method="post">
                    <input type="text" name="city" placeholder="City" required>
                    <input style="height: 50px; width: 100%;" type="date" name="time" placeholder="Time" required>
                    <input type="hidden" id="idPackage-input" name="idPackage" value=""/>
                    <input style="margin-top: 10px;" type="submit" class="login loginmodal-submit" value="Register">
                </form>
            </div>
        </div>
    </div>

    <!-- Modal for adding new package-->
    <div class="modal fade" id="add-package" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="loginmodal-container">
                <h1>Add Package</h1><br>
                <form action="/admin/add-package" method="post">
                    <select class="form-control" name="sender">
                        <option selected disabled>Sender</option>
                        <c:forEach items="${users}" var="user">
                            <option value="${user.username}">${user.username}</option>
                        </c:forEach>
                    </select>
                    <select style="margin-top: 8px;" class="form-control" name="receiver">
                        <option selected disabled>Receiver</option>
                        <c:forEach items="${users}" var="user">
                            <option value="${user.username}">${user.username}</option>
                        </c:forEach>
                    </select>
                    <input style="margin-top: 8px;" type="text" name="name" placeholder="Name">
                    <input type="text" name="description" placeholder="Description">
                    <input type="text" name="sender_city" placeholder="Sender City">
                    <input type="text" name="receiver_city" placeholder="Receiver City">
                    <input style="margin-top: 8px;" type="submit" class="login loginmodal-submit" value="Add Package">
                </form>
            </div>
        </div>
    </div>

</div>

<script>
    $('#register-package').on('show.bs.modal', function (e) {
        var idPackage = $(e.relatedTarget).data('id');
        $(e.currentTarget).find('input[name="idPackage"]').val(idPackage);

        $(this).find('#register-package form input #idPackage-input').val(idPackage);

    });
</script>

</body>
</html>
