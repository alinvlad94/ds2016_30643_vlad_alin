<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 12/12/2016
  Time: 11:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href='<c:url value="/css/bootstrap.css"/>' rel="stylesheet">
    <link href='<c:url value="/css/client.css"/>' rel="stylesheet">
    <script src='/js/jquery.js'></script>
    <script src='<c:url value="/js/bootstrap.js"/>'></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Client</title>
</head>
</head>
<body>

<div class="container">
    <div class="jumbotron" style="height : 1000px;
            box-shadow: 1px 2px 2px 1px #91a5ae;">
        <div class="well">
            <h3>Hello ${username}</h3>
            <div style="float:right">
                <a style="font-size: 18px;" href="/logout">Logout</a>
            </div>
        </div>
        <div class="box col-lg-8 col-md-8 col-sm-12 col-sm-12">
            <div style="overflow: scroll; height:220px">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Sender</th>
                        <th>Receiver</th>
                        <th>S City</th>
                        <th>R City</th>
                        <th>Tracked</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody style="height:150px; overflow-y:auto">
                    <c:forEach items="${packets}" var="packet">
                        <tr>
                            <td>${packet.name}</td>
                            <td>${packet.sender}</td>
                            <td>${packet.receiver}</td>
                            <td>${packet.senderCity}</td>
                            <td>${packet.receiverCity}</td>
                            <c:if test="${packet.tracking == true}">
                                <td>
                                    <a>Active</a>
                                </td>
                            </c:if>
                            <c:if test="${packet.tracking == false}">
                                <td>
                                    <a>Not Active</a>
                                </td>
                            </c:if>
                            <td><a href="/get-info/${packet.idPackage}">Info</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box col-lg-4 col-md-4 col-sm-12 col-sm-12">
            <div class="well">
                <form action="/get-specific-packet" method="get">
                    <input type="text" name="name" placeholder="Name of packet"/>
                    <input type="submit" value="Search"/>
                </form>
                <form action="/client" method="get">
                    <input type="submit" value="All Packets">
                </form>
            </div>
        </div>

        <c:if test="${!empty routes && !empty packet}">
            <div class="well col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="well-lg">
                    <h3>Descriere : <h4>${packet.description}</h4></h3>
                    <table>
                        <c:forEach var="route" items="${routes}">
                            <tr>
                                <td>${route.city}</td>
                                <td>
                                    <div style="width : 40px">
                                    </div>
                                </td>
                                <td>${route.time}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </c:if>

    </div>
</div>

</body>
</html>
