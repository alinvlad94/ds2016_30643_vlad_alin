package controller;

import controller.paths.PathsUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import service.loginservice.User;


/**
 * @author Alin - 12/19/2016.
 */
@Controller
@RequestMapping("/register")
public class RegisterController extends controller.generic.Controller {

    @RequestMapping(method = RequestMethod.GET)
    public String getRegisterPage(){
        return PathsUtil.REGISTER_PATH;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String register(Model model, @RequestParam("username") String username, @RequestParam("password") String password,
                           @RequestParam("adress") String adress, @RequestParam("phone") String phone ,
                           @RequestParam("email") String email, @RequestParam("role") String role ){
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setAdress(adress);
        user.setEmail(email);
        user.setPhone(phone);
        user.setRole(role);

        String response = loginService.register(user);
        if(response.equals("REGISTER_ERROR")){
            model.addAttribute("error", "Eroare la inregistrare, incercati din nou !");
            return PathsUtil.REGISTER_PATH;
        }

        return PathsUtil.LOGIN_PATH;
    }
}
