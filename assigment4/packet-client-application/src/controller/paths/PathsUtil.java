package controller.paths;

/**
 * @author Alin - 1/3/2017.
 */
public class PathsUtil {

    public static final String HOME_PATH = "home";
    public static final String LOGIN_PATH = "login";
    public static final String ADMIN_PATH = "admin";
    public static final String CLIENT_PATH = "client";
    public static final String ERROR_PATH = "error";
    public static final String DISPATCH_PATH = "dispatch";
    public static final String REDIRECT_PREFIX = "redirect:/";
    public static final String REGISTER_PATH = "register";

}
