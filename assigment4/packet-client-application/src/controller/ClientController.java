package controller;

import controller.paths.PathsUtil;
import model.ConvertedRoute;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import service.packetservice.CustomMap;
import service.packetservice.Packet;
import service.packetservice.Route;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Alin - 12/13/2016.
 */
@Controller
public class ClientController extends controller.generic.Controller{

    private String username = null;

    @RequestMapping(value = "/client", method = RequestMethod.GET)
    public String getClientHomePage(Model model, @ModelAttribute(value = "packet") Packet packet, @ModelAttribute("routes") ArrayList<Route> routes){
        this.username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        if(username != null && !StringUtils.isEmpty(username)){
            model.addAttribute("username", username);
        }
        if(packetService.getAllPacketsByUsername(username) != null){
            model.addAttribute("packets", packetService.getAllPacketsByUsername(username));
        }
        if(packet != null && routes.size() != 0){
            List<ConvertedRoute> convertedRoutes = new ArrayList<>();
            for (Route route : routes){
                ConvertedRoute conv = new ConvertedRoute();
                conv.setCity(route.getCity());
                conv.setTime(route.getTime().toGregorianCalendar().getTime().toString());
                convertedRoutes.add(conv);
            }
            model.addAttribute("packet", packet);
            model.addAttribute("routes", convertedRoutes);
        }
        return PathsUtil.CLIENT_PATH;
    }

    @RequestMapping(value = "/get-info/{packetId}")
    public
    String getInfoAboutPacket(@PathVariable("packetId") String packetId,final RedirectAttributes redirectAttributes){
        CustomMap map = packetService.checkStatus(Integer.parseInt(packetId));
        redirectAttributes.addFlashAttribute("packet", map.getPacket());
        redirectAttributes.addFlashAttribute("routes", map.getRoutes());
        return PathsUtil.REDIRECT_PREFIX + PathsUtil.CLIENT_PATH;
    }

    @RequestMapping(value = "/get-specific-packet", method = RequestMethod.GET)
    public String getSpecificPacket(@RequestParam("name") String name, Model model){
        model.addAttribute("packets", packetService.getAllPacketsByUsername(username)
                                    .stream().filter(e -> e.getName().contains(name)).collect(Collectors.toList()));
        return PathsUtil.CLIENT_PATH;
    }

}
