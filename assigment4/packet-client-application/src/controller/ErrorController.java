package controller;

import controller.paths.PathsUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Alin - 12/13/2016.
 */
@Controller
@RequestMapping("/error")
public class ErrorController {

    @RequestMapping(method = RequestMethod.GET)
    public String getErrorPage(){
        return PathsUtil.ERROR_PATH;
    }
}
