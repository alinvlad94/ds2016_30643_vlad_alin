package controller;

import controller.paths.PathsUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Alin - 12/11/2016.
 */
@Controller
public class HomeController extends controller.generic.Controller {

    @RequestMapping("/")
    public String getHome(Model model){
        return PathsUtil.LOGIN_PATH;
    }

    @RequestMapping("/login")
    public String getLogin(){
        return PathsUtil.LOGIN_PATH;
    }

    @RequestMapping(path = "/{anything}")
    public String getHomeDefault(){
        return PathsUtil.LOGIN_PATH;
    }
}
