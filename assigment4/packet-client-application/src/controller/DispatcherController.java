package controller;

import controller.paths.PathsUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Alin - 12/13/2016.
 */
@Controller
@RequestMapping("/dispatch")
public class DispatcherController {

    private static final String CLIENT = "ROLE_CLIENT";
    private static final String ADMIN = "ROLE_ADMIN";

    @RequestMapping(method = RequestMethod.GET)
    public String dispatch() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        boolean isClient = false;
        boolean isAdmin = false;
        while (auth.getAuthorities().iterator().hasNext()){
            String role = auth.getAuthorities().iterator().next().getAuthority();
            if(role.equals(CLIENT)){
                isClient = true;
                break;
            }else if(role.equals(ADMIN)){
                isAdmin = true;
                break;
            }
        }

        if (isClient) {
            return PathsUtil.REDIRECT_PREFIX + PathsUtil.CLIENT_PATH;
        } else if (isAdmin) {
            return PathsUtil.REDIRECT_PREFIX + PathsUtil.ADMIN_PATH;
        } else {
            return PathsUtil.ERROR_PATH;
        }
    }
}
