package controller;

import controller.paths.PathsUtil;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import service.loginservice.User;
import service.packetservice.Packet;
import service.packetservice.Route;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Alin - 12/13/2016.
 */
@Controller
@RequestMapping("/admin")
public class AdminController extends controller.generic.Controller{


    @RequestMapping(method = RequestMethod.GET)
    public String getAdminHomePage(Model model){
        if(packetService.getAllPackets() != null) {
            model.addAttribute("packets",packetService.getAllPackets());
        }
        if(loginService.getUsers() != null){
            List<User> usersFromDb = loginService.getUsers();
            List<User> users= usersFromDb.stream().filter(e -> e.getRole().equals("ROLE_CLIENT")).collect(Collectors.toList());
            model.addAttribute("users", users);
        }
        return PathsUtil.ADMIN_PATH;
    }

    @RequestMapping(value = "/add-package", method = RequestMethod.POST)
    public String addNewPackage(@RequestParam("sender") String sender, @RequestParam("receiver") String receiver,
                                @RequestParam("name") String name, @RequestParam("description") String description,
                                @RequestParam("sender_city") String senderCity, @RequestParam("receiver_city") String receiverCity){
        Packet packet = new Packet();
        packet.setSender(sender);
        packet.setReceiver(receiver);
        packet.setName(name);
        packet.setDescription(description);
        packet.setSenderCity(senderCity);
        packet.setReceiverCity(receiverCity);
        //set tracking to false
        packet.setTracking(false);

        //add new package
        //TODO : Error handling
        packetService.addPackage(packet);

        return PathsUtil.REDIRECT_PREFIX + PathsUtil.ADMIN_PATH;
    }

    @RequestMapping(value = "/delete-package/{id_packet}", method = RequestMethod.GET)
    public String deletePackage(@PathVariable String id_packet){
        packetService.removePackage(Integer.parseInt(id_packet));

        //TODO : Error handling
        return PathsUtil.REDIRECT_PREFIX + PathsUtil.ADMIN_PATH;
    }

    @RequestMapping(value = "/add-route", method = RequestMethod.POST)
    public String addNewRoute(@RequestParam("time") String time, @RequestParam("city") String city,
                              @RequestParam("idPackage") String idPackage) throws ParseException, DatatypeConfigurationException {

        Route route = new Route();
        route.setTime(getGregorianCalendar(time));
        route.setCity(city);
        packetService.addNewRouteToPackage(Integer.parseInt(idPackage),route);
        return PathsUtil.REDIRECT_PREFIX + PathsUtil.ADMIN_PATH;
    }

    public XMLGregorianCalendar getGregorianCalendar(String time) throws ParseException, DatatypeConfigurationException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(time);
        XMLGregorianCalendar xmlCalender=null;
        GregorianCalendar calender = new GregorianCalendar();
        calender.setTime(date);
        xmlCalender = DatatypeFactory.newInstance().newXMLGregorianCalendar(calender);
        return xmlCalender;
    }

}

