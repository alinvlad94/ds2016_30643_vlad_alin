package controller.generic;

import service.loginservice.LoginService;
import service.loginservice.LoginServiceService;
import service.packetservice.PacketService;
import service.packetservice.PacketServiceService;

import javax.annotation.PostConstruct;

/**
 * @author Alin - 12/20/2016.
 */
@org.springframework.stereotype.Controller
public abstract class Controller {

    protected LoginService loginService;
    protected PacketService packetService;

    @PostConstruct
    public void construct(){
        LoginServiceService stub1 = new LoginServiceService();
        loginService = stub1.getLoginServicePort();

        PacketServiceService stub2 = new PacketServiceService();
        packetService = stub2.getPacketServicePort();
    }
}
