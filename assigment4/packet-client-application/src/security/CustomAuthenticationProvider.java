package security;

import model.Role;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import service.loginservice.LoginService;
import service.loginservice.LoginServiceService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Alin - 12/12/2016.
 */
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private static final String USER_NOT_FOUND = "USER_NOT_FOUND";
    private static final String BAD_PASSWORD = "BAD_PASSWORD";
    private static final String COMMUNICATION_PROBLEM = "COMMUNICATION_PROBLEM";

    private LoginService loginService;

    public CustomAuthenticationProvider(){
        LoginServiceService stub = new LoginServiceService();
        loginService = stub.getLoginServicePort();
    }

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();

        String authenticationResponse = loginService.authenticate(username, password);
        if(authenticationResponse.equals(USER_NOT_FOUND)){
            throw new BadCredentialsException("Username not found.");
        }
        if(authenticationResponse.equals(BAD_PASSWORD)){
            throw new BadCredentialsException("Wrong password.");
        }

        if(authenticationResponse.equals(COMMUNICATION_PROBLEM)){
            throw new BadCredentialsException("Communication problems. Try again later !");
        }
        Role role = new Role(authenticationResponse);
        List<Role> authoritiesList = new ArrayList<Role>();
        authoritiesList.add(role);
        Collection<? extends GrantedAuthority> authorities = authoritiesList;

        return new UsernamePasswordAuthenticationToken(username, password, authorities);
    }

    public boolean supports(Class<?> aClass) {
        return true;
    }
}
