package model;

/**
 * @author Alin - 1/9/2017.
 */
public class ConvertedRoute {

    private String city;
    private String time;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
