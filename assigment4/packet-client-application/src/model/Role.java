package model;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Alin - 12/12/2016.
 */
public class Role implements GrantedAuthority{

    private String name;

    public Role(String roleName) {
        name = roleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthority() {
        return getName();
    }
}
