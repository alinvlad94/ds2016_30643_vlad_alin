
package service.packetservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the service.packetservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RegisterPackageResponse_QNAME = new QName("http://packetservice.service/", "registerPackageResponse");
    private final static QName _RemovePackageResponse_QNAME = new QName("http://packetservice.service/", "removePackageResponse");
    private final static QName _GetAllPacketsResponse_QNAME = new QName("http://packetservice.service/", "getAllPacketsResponse");
    private final static QName _AddNewRouteToPackageResponse_QNAME = new QName("http://packetservice.service/", "addNewRouteToPackageResponse");
    private final static QName _AddPackageResponse_QNAME = new QName("http://packetservice.service/", "addPackageResponse");
    private final static QName _AddNewRouteToPackage_QNAME = new QName("http://packetservice.service/", "addNewRouteToPackage");
    private final static QName _RemovePackage_QNAME = new QName("http://packetservice.service/", "removePackage");
    private final static QName _AddPackage_QNAME = new QName("http://packetservice.service/", "addPackage");
    private final static QName _GetAllPacketsByUsername_QNAME = new QName("http://packetservice.service/", "getAllPacketsByUsername");
    private final static QName _CheckStatusResponse_QNAME = new QName("http://packetservice.service/", "checkStatusResponse");
    private final static QName _GetAllPackets_QNAME = new QName("http://packetservice.service/", "getAllPackets");
    private final static QName _CheckStatus_QNAME = new QName("http://packetservice.service/", "checkStatus");
    private final static QName _GetAllPacketsByUsernameAndName_QNAME = new QName("http://packetservice.service/", "getAllPacketsByUsernameAndName");
    private final static QName _GetAllPacketsByUsernameAndNameResponse_QNAME = new QName("http://packetservice.service/", "getAllPacketsByUsernameAndNameResponse");
    private final static QName _RegisterPackage_QNAME = new QName("http://packetservice.service/", "registerPackage");
    private final static QName _GetAllPacketsByUsernameResponse_QNAME = new QName("http://packetservice.service/", "getAllPacketsByUsernameResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: service.packetservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddPackageResponse }
     * 
     */
    public AddPackageResponse createAddPackageResponse() {
        return new AddPackageResponse();
    }

    /**
     * Create an instance of {@link AddNewRouteToPackage }
     * 
     */
    public AddNewRouteToPackage createAddNewRouteToPackage() {
        return new AddNewRouteToPackage();
    }

    /**
     * Create an instance of {@link RemovePackage }
     * 
     */
    public RemovePackage createRemovePackage() {
        return new RemovePackage();
    }

    /**
     * Create an instance of {@link AddPackage }
     * 
     */
    public AddPackage createAddPackage() {
        return new AddPackage();
    }

    /**
     * Create an instance of {@link GetAllPacketsByUsername }
     * 
     */
    public GetAllPacketsByUsername createGetAllPacketsByUsername() {
        return new GetAllPacketsByUsername();
    }

    /**
     * Create an instance of {@link RegisterPackageResponse }
     * 
     */
    public RegisterPackageResponse createRegisterPackageResponse() {
        return new RegisterPackageResponse();
    }

    /**
     * Create an instance of {@link RemovePackageResponse }
     * 
     */
    public RemovePackageResponse createRemovePackageResponse() {
        return new RemovePackageResponse();
    }

    /**
     * Create an instance of {@link GetAllPacketsResponse }
     * 
     */
    public GetAllPacketsResponse createGetAllPacketsResponse() {
        return new GetAllPacketsResponse();
    }

    /**
     * Create an instance of {@link AddNewRouteToPackageResponse }
     * 
     */
    public AddNewRouteToPackageResponse createAddNewRouteToPackageResponse() {
        return new AddNewRouteToPackageResponse();
    }

    /**
     * Create an instance of {@link GetAllPackets }
     * 
     */
    public GetAllPackets createGetAllPackets() {
        return new GetAllPackets();
    }

    /**
     * Create an instance of {@link CheckStatus }
     * 
     */
    public CheckStatus createCheckStatus() {
        return new CheckStatus();
    }

    /**
     * Create an instance of {@link GetAllPacketsByUsernameAndName }
     * 
     */
    public GetAllPacketsByUsernameAndName createGetAllPacketsByUsernameAndName() {
        return new GetAllPacketsByUsernameAndName();
    }

    /**
     * Create an instance of {@link GetAllPacketsByUsernameAndNameResponse }
     * 
     */
    public GetAllPacketsByUsernameAndNameResponse createGetAllPacketsByUsernameAndNameResponse() {
        return new GetAllPacketsByUsernameAndNameResponse();
    }

    /**
     * Create an instance of {@link RegisterPackage }
     * 
     */
    public RegisterPackage createRegisterPackage() {
        return new RegisterPackage();
    }

    /**
     * Create an instance of {@link GetAllPacketsByUsernameResponse }
     * 
     */
    public GetAllPacketsByUsernameResponse createGetAllPacketsByUsernameResponse() {
        return new GetAllPacketsByUsernameResponse();
    }

    /**
     * Create an instance of {@link CheckStatusResponse }
     * 
     */
    public CheckStatusResponse createCheckStatusResponse() {
        return new CheckStatusResponse();
    }

    /**
     * Create an instance of {@link CustomMap }
     * 
     */
    public CustomMap createCustomMap() {
        return new CustomMap();
    }

    /**
     * Create an instance of {@link Packet }
     * 
     */
    public Packet createPacket() {
        return new Packet();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "registerPackageResponse")
    public JAXBElement<RegisterPackageResponse> createRegisterPackageResponse(RegisterPackageResponse value) {
        return new JAXBElement<RegisterPackageResponse>(_RegisterPackageResponse_QNAME, RegisterPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "removePackageResponse")
    public JAXBElement<RemovePackageResponse> createRemovePackageResponse(RemovePackageResponse value) {
        return new JAXBElement<RemovePackageResponse>(_RemovePackageResponse_QNAME, RemovePackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPacketsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "getAllPacketsResponse")
    public JAXBElement<GetAllPacketsResponse> createGetAllPacketsResponse(GetAllPacketsResponse value) {
        return new JAXBElement<GetAllPacketsResponse>(_GetAllPacketsResponse_QNAME, GetAllPacketsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddNewRouteToPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "addNewRouteToPackageResponse")
    public JAXBElement<AddNewRouteToPackageResponse> createAddNewRouteToPackageResponse(AddNewRouteToPackageResponse value) {
        return new JAXBElement<AddNewRouteToPackageResponse>(_AddNewRouteToPackageResponse_QNAME, AddNewRouteToPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "addPackageResponse")
    public JAXBElement<AddPackageResponse> createAddPackageResponse(AddPackageResponse value) {
        return new JAXBElement<AddPackageResponse>(_AddPackageResponse_QNAME, AddPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddNewRouteToPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "addNewRouteToPackage")
    public JAXBElement<AddNewRouteToPackage> createAddNewRouteToPackage(AddNewRouteToPackage value) {
        return new JAXBElement<AddNewRouteToPackage>(_AddNewRouteToPackage_QNAME, AddNewRouteToPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "removePackage")
    public JAXBElement<RemovePackage> createRemovePackage(RemovePackage value) {
        return new JAXBElement<RemovePackage>(_RemovePackage_QNAME, RemovePackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "addPackage")
    public JAXBElement<AddPackage> createAddPackage(AddPackage value) {
        return new JAXBElement<AddPackage>(_AddPackage_QNAME, AddPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPacketsByUsername }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "getAllPacketsByUsername")
    public JAXBElement<GetAllPacketsByUsername> createGetAllPacketsByUsername(GetAllPacketsByUsername value) {
        return new JAXBElement<GetAllPacketsByUsername>(_GetAllPacketsByUsername_QNAME, GetAllPacketsByUsername.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "checkStatusResponse")
    public JAXBElement<CheckStatusResponse> createCheckStatusResponse(CheckStatusResponse value) {
        return new JAXBElement<CheckStatusResponse>(_CheckStatusResponse_QNAME, CheckStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPackets }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "getAllPackets")
    public JAXBElement<GetAllPackets> createGetAllPackets(GetAllPackets value) {
        return new JAXBElement<GetAllPackets>(_GetAllPackets_QNAME, GetAllPackets.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "checkStatus")
    public JAXBElement<CheckStatus> createCheckStatus(CheckStatus value) {
        return new JAXBElement<CheckStatus>(_CheckStatus_QNAME, CheckStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPacketsByUsernameAndName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "getAllPacketsByUsernameAndName")
    public JAXBElement<GetAllPacketsByUsernameAndName> createGetAllPacketsByUsernameAndName(GetAllPacketsByUsernameAndName value) {
        return new JAXBElement<GetAllPacketsByUsernameAndName>(_GetAllPacketsByUsernameAndName_QNAME, GetAllPacketsByUsernameAndName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPacketsByUsernameAndNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "getAllPacketsByUsernameAndNameResponse")
    public JAXBElement<GetAllPacketsByUsernameAndNameResponse> createGetAllPacketsByUsernameAndNameResponse(GetAllPacketsByUsernameAndNameResponse value) {
        return new JAXBElement<GetAllPacketsByUsernameAndNameResponse>(_GetAllPacketsByUsernameAndNameResponse_QNAME, GetAllPacketsByUsernameAndNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "registerPackage")
    public JAXBElement<RegisterPackage> createRegisterPackage(RegisterPackage value) {
        return new JAXBElement<RegisterPackage>(_RegisterPackage_QNAME, RegisterPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPacketsByUsernameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://packetservice.service/", name = "getAllPacketsByUsernameResponse")
    public JAXBElement<GetAllPacketsByUsernameResponse> createGetAllPacketsByUsernameResponse(GetAllPacketsByUsernameResponse value) {
        return new JAXBElement<GetAllPacketsByUsernameResponse>(_GetAllPacketsByUsernameResponse_QNAME, GetAllPacketsByUsernameResponse.class, null, value);
    }

}
