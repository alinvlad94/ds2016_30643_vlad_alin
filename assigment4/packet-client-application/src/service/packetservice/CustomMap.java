
package service.packetservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for customMap complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="customMap">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="packet" type="{http://packetservice.service/}packet" minOccurs="0"/>
 *         &lt;element name="routes" type="{http://packetservice.service/}route" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customMap", propOrder = {
    "packet",
    "routes"
})
public class CustomMap {

    protected Packet packet;
    @XmlElement(nillable = true)
    protected List<Route> routes;

    /**
     * Gets the value of the packet property.
     * 
     * @return
     *     possible object is
     *     {@link Packet }
     *     
     */
    public Packet getPacket() {
        return packet;
    }

    /**
     * Sets the value of the packet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Packet }
     *     
     */
    public void setPacket(Packet value) {
        this.packet = value;
    }

    /**
     * Gets the value of the routes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the routes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRoutes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Route }
     * 
     * 
     */
    public List<Route> getRoutes() {
        if (routes == null) {
            routes = new ArrayList<Route>();
        }
        return this.routes;
    }

}
