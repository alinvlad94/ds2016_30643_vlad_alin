package utils;

import model.CustomPackage;
import service.packetservice.Packet;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alin - 1/4/2017.
 */
public class Transformers {

    public static List<CustomPackage> transformListOfPackages(List<Packet> packets){
        List<CustomPackage> custom = new ArrayList<>();
        for(Packet packet: packets){
            CustomPackage newPackage = new CustomPackage();
            newPackage.setName(packet.getName());
            newPackage.setReceiver(packet.getReceiver());
            newPackage.setDescription(packet.getDescription());
            newPackage.setSender(packet.getSender());
            newPackage.setReceiver_city(packet.getReceiverCity());
            newPackage.setSender_city(packet.getSenderCity());
            newPackage.setTracking(packet.isTracking());
            custom.add(newPackage);
        }
        return custom;
    }
}
