package dao.datasource;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * @author Alin - 12/12/2016.
 */
public class DataSource extends DriverManagerDataSource {

    public DataSource(){
        setDriverClassName("com.mysql.jdbc.Driver");
        setUsername("root");
        setPassword("root");
        setUrl("jdbc:mysql://localhost:3306/assigment4");
    }
}
