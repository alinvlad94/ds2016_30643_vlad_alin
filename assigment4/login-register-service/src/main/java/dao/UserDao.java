package dao;

import dao.datasource.DataSource;
import model.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Alin - 12/11/2016.
 */

public class UserDao {

    private static final String USER_NOT_FOUND = "USER_NOT_FOUND";
    private static final String BAD_PASSWORD = "BAD_PASSWORD";
    private static final String COMMUNICATION_PROBLEM = "COMMUNICATION_PROBLEM";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private DataSource dataSource;

    public UserDao() {
        dataSource = new DataSource();
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public String checkAuthentication(String username, String password) {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource()
                    .addValue("username", username)
                    .addValue("password", password);
            List<User> users = namedParameterJdbcTemplate.query("SELECT * FROM user WHERE username = :username", params, new UserRowMapper());
            if (users.size() > 0) {
                if (users.get(0).getPassword().equals(password)) {
                    return users.get(0).getRole();
                } else {
                    return BAD_PASSWORD;
                }
            } else {
                return USER_NOT_FOUND;
            }
        } catch (Exception e) {
            return COMMUNICATION_PROBLEM;
        }
    }

    public String register(User user) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("username", user.getUsername())
                .addValue("password", user.getPassword())
                .addValue("adress", user.getAdress())
                .addValue("phone", user.getPhone())
                .addValue("email", user.getEmail())
                .addValue("role", user.getRole());
        try {
            namedParameterJdbcTemplate.update("INSERT INTO user(username, password, adress, phone, email, role)  VALUES(:username, :password, :adress,:phone, :email, :role);", params);
            return "REGISTER_SUCCESS";
        } catch (Exception e) {
            return "REGISTER_ERROR";
        }

    }

    public List<User> getAllUsers() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        List<User> users = namedParameterJdbcTemplate.query("SELECT * FROM user;", params, new UserRowMapper());
        return users;
    }

    class UserRowMapper implements RowMapper<User> {

        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user = new User();
            user.setUsername(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
            user.setAdress(resultSet.getString("adress"));
            user.setEmail(resultSet.getString("email"));
            user.setPhone(resultSet.getString("phone"));
            user.setRole(resultSet.getString("role"));
            return user;
        }
    }
}
