package launcher;

import dao.UserDao;
import service.loginservice.LoginService;

import javax.xml.ws.Endpoint;

/**
 * @author Alin - 12/12/2016.
 */
public class LoginRegisterLauncher {

    public static void main(String[] argv) {

        Object loginService = new LoginService();
        String address2 = "http://localhost:9001/LoginService";

        Endpoint.publish(address2, loginService);
    }
}
