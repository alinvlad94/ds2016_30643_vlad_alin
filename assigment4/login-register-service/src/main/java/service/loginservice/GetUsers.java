
package service.loginservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getUsers", namespace = "http://loginservice.service/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUsers", namespace = "http://loginservice.service/")
public class GetUsers {


}
