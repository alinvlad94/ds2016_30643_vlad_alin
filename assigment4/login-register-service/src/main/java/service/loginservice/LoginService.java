package service.loginservice;

import dao.UserDao;
import model.User;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * @author Alin - 12/11/2016.
 */
@WebService
public class LoginService {

    private UserDao userDao = new UserDao();

    @WebMethod
    public String register(User user){
        return userDao.register(user);
    }

    @WebMethod
    public String authenticate(String username, String password){
        return userDao.checkAuthentication(username, password);
    }

    @WebMethod
    public List<User> getUsers(){
        return userDao.getAllUsers();
    }
}
