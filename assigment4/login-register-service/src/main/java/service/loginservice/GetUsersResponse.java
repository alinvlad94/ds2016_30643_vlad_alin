
package service.loginservice;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getUsersResponse", namespace = "http://loginservice.service/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUsersResponse", namespace = "http://loginservice.service/")
public class GetUsersResponse {

    @XmlElement(name = "return", namespace = "")
    private List<model.User> _return;

    /**
     * 
     * @return
     *     returns List<User>
     */
    public List<model.User> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<model.User> _return) {
        this._return = _return;
    }

}
