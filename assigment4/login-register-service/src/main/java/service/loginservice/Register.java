
package service.loginservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "register", namespace = "http://loginservice.service/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "register", namespace = "http://loginservice.service/")
public class Register {

    @XmlElement(name = "arg0", namespace = "")
    private model.User arg0;

    /**
     * 
     * @return
     *     returns User
     */
    public model.User getArg0() {
        return this.arg0;
    }

    /**
     * 
     * @param arg0
     *     the value for the arg0 property
     */
    public void setArg0(model.User arg0) {
        this.arg0 = arg0;
    }

}
