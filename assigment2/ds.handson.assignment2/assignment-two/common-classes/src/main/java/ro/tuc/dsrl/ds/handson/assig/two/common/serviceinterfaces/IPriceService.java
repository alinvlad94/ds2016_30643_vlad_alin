package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

/**
 * Created by Alin on 11/9/2016.
 */
public interface IPriceService {

    /**
     * Computes the price of a car depending on the purchesing price and the year of fabrication
     * @param c Car for which to compute the price
     * @return price for the car
     */
    double computePrice(Car c);
}
