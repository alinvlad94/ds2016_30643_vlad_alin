package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;

/**
 * Created by Alin on 11/9/2016.
 */
public class PriceService implements IPriceService {
    @Override
    public double computePrice(Car c) {
        if(c.getPrice() < 0.0){
            if (c.getEngineCapacity() <= 0) {
                throw new IllegalArgumentException("Price must be positive.");
            }
        }
        // Formula for calculating the selling price depending on the purchesing price and the fabrication year
        double price = c.getPrice() - c.getPrice() / 7 * (2015 - c.getYear());
        if (price < 0.0) {
            price = -1 * price;
        }
        return price;
    }
}
