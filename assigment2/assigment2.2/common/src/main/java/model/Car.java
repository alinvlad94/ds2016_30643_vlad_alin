package model;

import java.io.Serializable;

/**
 * Created by Alin on 11/13/2016.
 */
public class Car implements Serializable {

    private int year;
    private int engine_size;
    private double purchasing_price;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngine_size() {
        return engine_size;
    }

    public void setEngine_size(int engine_size) {
        this.engine_size = engine_size;
    }

    public double getPurchasing_price() {
        return purchasing_price;
    }

    public void setPurchasing_price(double purchasing_price) {
        this.purchasing_price = purchasing_price;
    }
}
