package inteface;

import model.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Alin - 11/13/2016.
 */
public interface PriceComputer extends Remote{

    double computePrice(Car c) throws RemoteException;
}
