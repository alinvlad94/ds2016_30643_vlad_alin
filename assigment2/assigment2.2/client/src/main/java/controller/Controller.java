package controller;

import inteface.PriceComputer;
import inteface.TaxComputer;
import model.Car;
import view.ComputerView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

/**
 * @author Alin - 11/13/2016.
 */
public class Controller {

    private PriceComputer priceComputer;
    private TaxComputer taxComputer;

    private ComputerView view;

    public Controller(PriceComputer priceComputer, TaxComputer taxComputer) {
        view = new ComputerView();
        view.setVisible(true);

        this.priceComputer = priceComputer;
        this.taxComputer = taxComputer;
        view.setGetInfoFromPriceServiceListener(new GetInfoFromPriceServiceListener());
        view.setGetInfoFromTaxServiceListener(new GetInfoFromTaxServiceListener());
    }

    public class GetInfoFromPriceServiceListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                Car car = view.getCarInfoFromInputs();
                double price = priceComputer.computePrice(car);
                view.printInfo(price);
            } catch (RemoteException e1) {

            }
        }
    }

    public class GetInfoFromTaxServiceListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            try {
                Car car = view.getCarInfoFromInputs();
                double tax = taxComputer.computeTax(car);
                view.printInfo(tax);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }

        }
    }
}
