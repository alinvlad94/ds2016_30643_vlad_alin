package view;

import controller.Controller;
import model.Car;

import javax.swing.*;

/**
 * @author Alin - 11/13/2016.
 */
public class ComputerView extends JFrame {

    JButton priceServiceButton;
    JButton taxServiceButton;
    JTextArea result;
    JPanel contentPane;
    JLabel yearLabel, engineLabel, priceLabel;
    JTextArea year, engine, price;

    public ComputerView() {
        setTitle("Tax & Price Application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 500, 300);
        contentPane = new JPanel();
        contentPane.setLayout(null);

        taxServiceButton = new JButton("Tax");
        priceServiceButton = new JButton("Price");
        result = new JTextArea();
        yearLabel = new JLabel("Year");
        engineLabel = new JLabel("Engine");
        priceLabel = new JLabel("Price");
        year = new JTextArea();
        engine = new JTextArea();
        price = new JTextArea();

        yearLabel.setBounds(10, 20, 50, 30);
        engineLabel.setBounds(10, 70, 50, 30);
        priceLabel.setBounds(10, 120, 50, 30);

        year.setBounds(100, 20, 70, 30);
        engine.setBounds(100, 70, 70, 30);
        price.setBounds(100, 120, 70, 30);
        year.setBorder(BorderFactory.createBevelBorder(1));
        engine.setBorder(BorderFactory.createBevelBorder(1));
        price.setBorder(BorderFactory.createBevelBorder(1));

        priceServiceButton.setBounds(200, 20, 100, 50);
        taxServiceButton.setBounds(200, 90, 100, 50);
        result.setBounds(350, 20, 100, 100);
        result.setBorder(BorderFactory.createBevelBorder(1));

        contentPane.add(priceServiceButton);
        contentPane.add(taxServiceButton);

        contentPane.add(year);
        contentPane.add(engine);
        contentPane.add(price);

        contentPane.add(yearLabel);
        contentPane.add(engineLabel);
        contentPane.add(priceLabel);

        contentPane.add(result);

        setContentPane(contentPane);
    }

    public void setGetInfoFromPriceServiceListener(Controller.GetInfoFromPriceServiceListener getInfoFromPriceServiceListener) {
        priceServiceButton.addActionListener(getInfoFromPriceServiceListener);
    }

    public void setGetInfoFromTaxServiceListener(Controller.GetInfoFromTaxServiceListener getInfoFromTaxServiceListener) {
        taxServiceButton.addActionListener(getInfoFromTaxServiceListener);
    }

    public void printInfo(double number) {
        result.setText(number + "");
    }

    public Car getCarInfoFromInputs() {
        Car car = new Car();
        if (year.getText() != null && engine.getText() != null && price.getText() != null) {
            try {
                car.setYear(Integer.parseInt(year.getText()));
                car.setEngine_size(Integer.parseInt(engine.getText()));
                car.setPurchasing_price(Integer.parseInt(price.getText()));
                return car;
            } catch (Exception e) {
                displayErrorMessageForInput("Introduceti numere valide");
            }
        } else {
            displayErrorMessageForInput("Completati toate campurile");
        }
        return car;
    }

    public void displayErrorMessageForInput(String message) {
        clear();
        JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void clear() {
        year.setText("");
        engine.setText("");
        price.setText("");
        result.setText("");
    }
}
