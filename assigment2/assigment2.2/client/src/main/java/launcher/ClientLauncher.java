package launcher;

import controller.Controller;
import inteface.PriceComputer;
import inteface.TaxComputer;
import model.Car;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Alin on 11/13/2016.
 */
public class ClientLauncher {

    private static final String PRICE_COMPUTER = "PriceComputer";
    private static final String TAX_COMPUTER = "TaxComputer";

    public static void main(String[] args) throws RemoteException, NotBoundException {

        Registry registry = LocateRegistry.getRegistry(1099);
        PriceComputer priceComputer = (PriceComputer) registry.lookup(PRICE_COMPUTER);
        TaxComputer taxComputer = (TaxComputer) registry.lookup(TAX_COMPUTER);
        new Controller(priceComputer, taxComputer);
    }


}