package launcher;

import inteface.PriceComputer;
import inteface.TaxComputer;
import service.PriceComputerImpl;
import service.TaxComputerImpl;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.Permission;

/**
 * @author Alin - 11/13/2016.
 */
public class ServerLauncher {

    private static final String PRICE_COMPUTER = "PriceComputer";
    private static final String TAX_COMPUTER = "TaxComputer";

    public static void main(String[] args){

        if(System.getSecurityManager() == null){
            System.setSecurityManager(new MySecurityManager());
        }

        System.setProperty("java.rmi.server.hostname","localhost");

        try{
            Registry registry = LocateRegistry.createRegistry(1099);

            PriceComputer priceComputer = new PriceComputerImpl();
            PriceComputer stubPrice = (PriceComputer) UnicastRemoteObject.exportObject(priceComputer, 0);

            TaxComputer taxComputer = new TaxComputerImpl();
            TaxComputer stubTax = (TaxComputer) UnicastRemoteObject.exportObject(taxComputer, 0);

            registry.bind(PRICE_COMPUTER, stubPrice);
            registry.bind(TAX_COMPUTER, stubTax);

            System.out.println("Server started...");

        }catch (Exception e){
            System.out.println("Server encountering exception");
        }
    }

    static class MySecurityManager extends SecurityManager{

        @Override
        public void checkPermission(Permission perm) {
            return;
        }

    }
}
