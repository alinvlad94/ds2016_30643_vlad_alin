package service;

import inteface.PriceComputer;
import model.Car;

/**
 * @author Alin - 11/13/2016.
 */
public class PriceComputerImpl implements PriceComputer {

    public PriceComputerImpl(){
        super();
    }

    public double computePrice(Car c) {
        if (c.getEngine_size() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        if(c.getPurchasing_price() < 0.0){
            if (c.getEngine_size() <= 0) {
                throw new IllegalArgumentException("Price must be positive.");
            }
        }
        // Formula for calculating the selling price depending on the purchesing price and the fabrication year
        double price = c.getPurchasing_price() - c.getPurchasing_price() / 7 * (2015 - c.getYear());
        if (price < 0.0) {
            price = -1 * price;
        }
        return price;
    }
}
