package service;

import inteface.TaxComputer;
import model.Car;

/**
 * @author Alin - 11/13/2016.
 */
public class TaxComputerImpl implements TaxComputer {

    public double computeTax(Car c) {
        // Dummy formula
        if (c.getEngine_size() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngine_size() > 1601) sum = 18;
        if(c.getEngine_size() > 2001) sum = 72;
        if(c.getEngine_size() > 2601) sum = 144;
        if(c.getEngine_size() > 3001) sum = 290;
        return c.getEngine_size()/ 200.0 * sum;
    }
}
