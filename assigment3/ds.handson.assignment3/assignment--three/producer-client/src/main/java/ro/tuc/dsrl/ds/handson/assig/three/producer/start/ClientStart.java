package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.queue.model.DVD;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);

		try {
            for(DVD dvd : constructListOfDvds()){
                queue.writeMessage(dvd);
            }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    public static List<DVD> constructListOfDvds(){
        List<DVD> list = new ArrayList<DVD>();
        list.add(new DVD("Titanic", 1966, 25));
        list.add(new DVD("Fast and Fourious", 2001, 23));
        list.add(new DVD("Catch me if you can", 1995, 28));
        list.add(new DVD("White men can't jump", 1988, 31));
        return list;
    }
}
