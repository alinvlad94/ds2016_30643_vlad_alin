package consumer.start;

import consumer.connection.QueueServerConnection;
import consumer.service.FileWriterService;
import ro.tuc.dsrl.ds.handson.assig.three.queue.model.DVD;

import java.io.IOException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientStart {

    private final static String NEW_LINE = System.getProperty("line.separator");

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost",8888);

		DVD message = null;

        FileWriterService fileWriter = new FileWriterService();

		while(true) {
			try {
				message = queue.readMessage();
				System.out.println("Writing in folder message "+constructMessage(message));
				fileWriter.writeInFile(constructMessage(message));
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

    private static String constructMessage(DVD message) {
        StringBuilder builder = new StringBuilder();
        builder.append("Title : ").append(message.getTitle()).append(NEW_LINE)
                .append("Year : ").append(message.getYear()).append(NEW_LINE)
                .append("Price : ").append(message.getPrice()).append(NEW_LINE)
                .append("By AV Movie Shop").append(NEW_LINE);
        return builder.toString();
    }
}
