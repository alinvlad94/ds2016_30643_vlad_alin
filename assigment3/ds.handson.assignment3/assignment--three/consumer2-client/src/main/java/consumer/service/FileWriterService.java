package consumer.service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;

/**
 * @author Alin - 11/23/2016.
 */
public class FileWriterService {

    File file;

    public FileWriterService(){
        file = new File("E:\\SistemeDistribuite\\assigment3\\ds.handson.assignment3\\assignment--three\\consumer2-client\\output\\output.txt");
    }

    public void writeInFile(String message){

        try {

            java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            // write in file
            bw.write(message);
            // close connection
            bw.close();
        } catch (FileNotFoundException e) {
            System.out.println("Problem writing in file");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
