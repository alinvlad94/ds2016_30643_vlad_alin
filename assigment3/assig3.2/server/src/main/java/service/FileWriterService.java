package service;

import java.io.*;

/**
 * @author Alin - 11/29/2016.
 */
public class FileWriterService {

    private File rootFile;

    public FileWriterService(){
        rootFile = new File("E:\\SistemeDistribuite\\assigment3\\assig3.2\\server\\output");
    }

    public void writeInFile(String message){

        try {
            File file = fetchNextFile();
            java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            // write in file
            bw.write(message);
            // close connection
            bw.close();
        } catch (FileNotFoundException e) {
            System.out.println("Problem writing in file");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File fetchNextFile() throws IOException {
        int max = 1;
        if(rootFile.getAbsoluteFile().listFiles().length == 0){
            return new File(rootFile + "\\" + "output_1.txt");
        }else {
            for (File file : rootFile.getAbsoluteFile().listFiles()) {
                if (getNumberOfFile(file.getName()) > max){
                    max = getNumberOfFile(file.getName());
                }
            }
        }
        return new File(rootFile + "\\" + "output" + "_" + ( max + 1 ) + ".txt");
    }


        private int getNumberOfFile(String fileName){
            String[] splitAfterUnderscore = fileName.split("_");
            String[] splitAfterDot = splitAfterUnderscore[1].split("\\.");
            return Integer.parseInt(splitAfterDot[0]);
        }
}
