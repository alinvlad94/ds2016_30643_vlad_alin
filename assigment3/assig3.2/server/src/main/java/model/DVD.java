package model;

import java.io.Serializable;

/**
 * @author Alin - 11/29/2016.
 */
public class DVD implements Serializable {

    private String title;
    private int year;
    private double price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    // Applying the Builder Design Pattern

    public DVD withTitle(String title) {
        this.title = title;
        return this;
    }

    public DVD withYear(int year) {
        this.year = year;
        return this;
    }

    public DVD withPrice(double price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString(){
        return "Title : " + title + " Year : " + year + " Price : "+ price;
    }
}
