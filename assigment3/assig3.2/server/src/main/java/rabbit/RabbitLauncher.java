package rabbit;

import com.rabbitmq.client.*;

/**
 * @author Alin - 12/8/2016.
 */
public class RabbitLauncher {

    private static final String EXCHANGE_NAME = "dvds";
    private static final String QUEUE_NAME_MAILSENDER = "queue_mailsender";
    private static final String QUEUE_NAME_FILEWRITER = "queue_filewriter";

    public static void main(String[] argv) throws Exception {


        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        //set the queue for mailsender consumer
        channel.queueDeclare(QUEUE_NAME_MAILSENDER, true, false, false, null);
        channel.queueBind(QUEUE_NAME_MAILSENDER, EXCHANGE_NAME, "");

        //set the queue for mailsender consumer
        channel.queueDeclare(QUEUE_NAME_FILEWRITER, true, false, false, null);
        channel.queueBind(QUEUE_NAME_FILEWRITER, EXCHANGE_NAME, "");

        System.out.println(" Set up the queues");

    }
}
