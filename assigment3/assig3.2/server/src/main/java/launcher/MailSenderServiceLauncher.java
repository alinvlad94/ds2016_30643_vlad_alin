package launcher;

import com.rabbitmq.client.*;
import service.EmailSenderService;

import java.io.IOException;

public class MailSenderServiceLauncher {

    private static final String EXCHANGE_NAME = "dvds";
    private static final String QUEUE_NAME = "queue_mailsender";
    private static EmailSenderService emailSenderService = new EmailSenderService("alinvladtest@gmail.com","alinvladtest1");

    //RabbitMQ connection beans
    private static ConnectionFactory factory;
    private static Connection connection;
    private static Channel channel;

    public static void main(String[] argv) throws Exception {

        setUpConnection();

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
                emailSenderService.sendMail(message);
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }

    //Set up rabbitmq connection to exchange
    public static void setUpConnection() throws IOException {

        factory = new ConnectionFactory();
        factory.setHost("localhost");

        connection = factory.newConnection();
        channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        System.out.println(" [*] Waiting for messages...");
    }
}
