<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 11/29/2016
  Time: 7:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/resources/jquery.js"></script>
    <script src="/resources/bootstrap.js"></script>
    <link href="/resources/bootstrap.css" rel="stylesheet">
    <title>DVD Store</title>
    <style>
        .jumbotron {
            margin: 16%;
            height: 60%;
            box-shadow: #6f706b 3px 1px 1px 3px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="jumbotron">
        <form action="/post.form" method="post" <%--onsubmit="postData(title, year, price)"--%>>
            <div class="form-group row">
                <div class="col-lg-3"></div>
                <label for="title" class="col-xs-1 col-form-label">Title</label>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <input class="form-control" type="text" name="title" id="title" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3"></div>
                <label for="year" class="col-xs-1 col-form-label">Year</label>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <input class="form-control" type="text" name="year" id="year" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3"></div>
                <label for="price" class="col-xs-1 col-form-label">Price</label>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <input class="form-control" type="text" name="price" id="price" required>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-4"></div>
            <input type="submit" id="submitButton" class="btn btn-lg btn-success" value="Add"/><br>
            <center><h3>${response}</h3></center>
        </form>
    </div>
</div>

<script>
    $(document).ready(
            function () {
                $('#submitButton').click(function () {
                    var title = $('#title').val();
                    var year = $('#year').val();
                    var price = $('#price').val();
                    if(title.length != 0 && year.length != 0 && price.length != 0) {
                        postData(title, year, price);
                    }
                });
            }
    );

    function postData(title, year, price) {

        var data = {
            "title" : title,
            "year" : year,
            "price" : price
        };

        $.ajax({
            type: "POST",
            url: "postData.form",
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data),

            success: function (response) {
                $("#response").text(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
</script>

</body>
</html>
