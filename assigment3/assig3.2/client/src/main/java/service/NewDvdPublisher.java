package service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import model.DVD;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author Alin - 11/29/2016.
 */
@Service
public class NewDvdPublisher {

    private static final String EXCHANGE_NAME = "dvds";
    private Channel channel;
    private Connection connection;

    public String publish(DVD dvd) throws IOException {

        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

            String message = dvd.toString();

            channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
            System.out.println(" [x] Sent '" + message + "' with success");

            return "Published with success !";

        } catch (IOException e) {
            System.out.println("UNSUCCESSFULL PUBLISHING ON RABBIT" + e.getMessage());
            return "Publishing failed ! Try again !";
        }finally {
            channel.close();
            connection.close();
        }





    }


}
