package controller;

import model.DVD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.NewDvdPublisher;

import java.io.IOException;
import java.util.Map;

/**
 * @author Alin - 11/29/2016.
 */

@RestController
public class ClientController{

    private NewDvdPublisher newDvdPublisher;

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public ModelAndView postNewDvd(@RequestParam("title")String title, @RequestParam("year") int year, @RequestParam("price") double price, Model model) {
        DVD dvd = new DVD().withTitle(title)
                .withYear(year)
                .withPrice(price);
        String response;
        try {
            response = newDvdPublisher.publish(dvd);
            model.addAttribute("response", response);
        } catch (IOException e) {
            model.addAttribute("response", "Unsuccesful publishing");
        }
        return new ModelAndView("index.jsp");
    }

    @RequestMapping(value = "/postData", method = RequestMethod.POST, consumes = "application/json")
    public String postNewDvd(@RequestBody DVD dvd) {
        System.out.println(dvd);
        return "aaa";
    }

    @Autowired
    public void setNewDvdPublisher(NewDvdPublisher newDvdPublisher) {
        this.newDvdPublisher = newDvdPublisher;
    }
}
